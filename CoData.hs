
module CoData where

import Prelude hiding (head,tail,take,repeat)
import qualified Prelude as P (take)
-- This package has to be installed.  If you're on ubuntu it can be installed with sudo apt install libghc-control-monad-free-dev.  However, don't 
-- install this package; it's old.  Instead use libghc-free-dev.
-- It can also be installed with stack install or cabal install.  On Windows, it can be installed by using WSL2 and installing Ubuntu and then using the above.
-- Alternatively, comment out the Generator stuff.
import Control.Monad.Free hiding (unfold)
-- However, the implementation of Generator using Free monads is slow, because the diagonalization takes a while.  The trick 
-- to asymptotically speed-up Free monads is to make it a codensity monad instead.  See 
-- https://www.janis-voigtlaender.eu/papers/AsymptoticImprovementOfComputationsOverFreeMonads.pdf
--  We'll use this speed-up at the end. 
-- Control.Monad.Codensity comes from the kan-extensions package (libghc-kan-extensions-dev on ubuntu).  However, note, 
-- this pulls in a lot of dependencies.  So feel free to comment this bit out, and also comment out the bits that use Codensity.
import Control.Monad.Codensity



-- Usually in Codata one has destructors and unfold.  We'll follow this method using record types in Haskell.
data Stream a = S
    {
        head :: a,
        tail :: Stream a 
    }

unfold :: (c -> a) -> (c -> c) -> c -> Stream a 
unfold h t v = S {head = h v, tail = unfold h t (t v)}

ones :: Stream Integer
ones = unfold id id 1

zeros :: Stream Integer 
zeros = unfold id id 0

nats :: Stream Integer  
nats = unfold id (+1) 0

take :: Int -> Stream a -> [a]
take 0 _s = []
take n s = (head s) : take (n-1) (tail s)

streamID :: Stream a -> Stream a 
streamID s = unfold head tail s

-- Use only unfold to do everything!
mapStream :: (a -> b) -> Stream a -> Stream b 
-- head :: Stream a -> a  and f :: a -> b so f . head :: Stream a -> b and tail :: Stream a -> Stream a
mapStream f s = unfold (f . head) tail s

positiveNats :: Stream Integer 
positiveNats = mapStream (+1) nats

instance Functor Stream where 
    fmap = mapStream

-- Next, note that Streams are also applicative -- pure :: a -> Stream a and <*> 
repeat :: a -> Stream a 
repeat x = unfold id id x 

-- I don't know whether the "direct" or the unfold definition is "easier" to understand
-- I left the definition as using unfold.
zipStream :: Stream a -> Stream b -> Stream (a,b)
--zipStream s1 s2 = S {head = (head s1,head s2),tail = zipStream (tail s1) (tail s2)}
zipStream s1 s2 = unfold (\(s1',s2') -> (head s1',head s2')) (\(s1',s2') -> (tail s1',tail s2')) (s1,s2)

-- Given a stream of functions (f0,f1,...) and a stream (a0,...) we can make a stream (f0 a0, f1 a1, ...)
-- This is essentially zipping and evaluating
streamMon :: Stream (a -> b) -> Stream a -> Stream b 
streamMon fs as = unfold (\(s1,s2) -> (head s1) (head s2)) (\(s1,s2) -> (tail s1, tail s2)) (fs,as)

-- This is the stream of functions (+0),(+1),(+2),...
-- The tail function keeps the sum to n going and the head function creates the function that does \x -> x+n
plusNats :: Stream (Integer -> Integer) 
plusNats = unfold (\n -> (+n)) (+1) 0

-- We should get the stream of nats by applying plusNats to zeros 
-- To prove this is the case, use bisimulation
nats2 :: Stream Integer
nats2 = streamMon plusNats zeros

instance Applicative Stream where 
    pure = repeat 
    fs <*> as = streamMon fs as

-- There are different ways to flatten a stream of streams.  However, there is only one way to do so monadically.
-- Argue via bisimulation that join takes the first element of the first stream, the second element of the second stream, and so on...  i.e. it's the diagonal.
joinS :: Stream (Stream a) -> Stream a 
joinS s = unfold (head . head) (fmap tail) s

instance Monad Stream where 
    return = pure 
    s >>= f = joinS (fmap f s)

-- Exercise: prove by bisimulation that if we derive <*> from Monad by 
-- do {f <- fs, a <- as, return (f a)}
-- That we get the <*> defined above.

-- Okay, I admit, Streams aren't *quite* the codata type we want, in practice.
-- For implementing RNNs in practice, we probably want something more like Mealy Machines or Generators.  
-- We'll discuss these in turn next.  Mealy machines are like agents or objects but have an infinite life.  Generators 
-- are Mealy machines that are forced to form a monad.  More precisely Mealy Machines embed into Generators 
-- and Generators form a (free) monad.

{-
As an exercise, first, we'll dress up stateful computations as codata.
-}
data State s a = State {runState :: s -> (a,s)}

-- Note that we could have done state with two projections
nextState :: (State s a) -> s -> s 
nextState m s = snd $ (runState m) s 
nextVal :: (State s a ) -> s -> a 
nextVal m s = fst $ (runState m) s

-- State is the codata type with respect to the above. 
-- Thus to unfold a state from b is to give  
-- ns :: b -> (s -> s)  
-- nv :: b -> (s -> a)
-- However, note that this is exactly equivalent to giving 
-- nsv :: b -> (s -> s, s -> a)
-- Exercise: show that for any x,y,z that mx :: (x -> y) -> (x -> z) -> x -> (y,z) given by mx f g x = (f x,g x) is invertible.
-- But then note that to give something with the type of nsv is exactly to give something of the type 
-- nsv2 :: b -> (s -> (s,a))  -- this is given by applying the exercise a second time. 
-- Thus to unfold state, we can take a single function (b -> (s -> (a,s))) and something from b and get back a State s a thing.
unfoldState :: (b -> (s -> (a,s))) -> b -> State s a 
unfoldState f b = State {runState = f b}


-- Here we'll write instances of functor, applicative, and monad, using unfold, similar to how we did it for streams.
-- We just need to unfold a term of type (State s a -> (s -> (b,s)))
mapState :: (a -> b) -> State s a -> State s b
mapState f s = unfoldState (\curS -> (\s1 -> let (y,s2) = (runState curS) s1 in (f y,s2))) s 

instance Functor (State s) where 
    fmap = mapState

initState :: a -> State s a 
initState a = unfoldState (\v -> \s -> (v,s)) a
-- Show that initState a = State {runState s = (a,s)}

-- Given a statefully computed function and a statefully computed input, statefully compute its output.
-- Okay, so this is a bit of an annoying part of the statemonad -- it's not commutative.  This means that in do notation 
-- we don't have do {x <- a,y <- b, whatever} == do {y <- b, x <- a, whatever}.  This means that there are in fact 
-- 2 ways to do the <*> operation.  One where you obtain f <- sf first and another where you obtain a <- sa first. 
-- Here we will take f <- sf first.  To make this more readable, we will use do notation.
stateMon :: State s (a -> b) -> State s a -> State s b
stateMon sf sa = do 
    f <- sf 
    a <- sa 
    return (f a)

instance Applicative (State s) where 
    pure = initState
    sf <*> sa = stateMon sf sa 

-- Realistically, unfold is unnecessary here, but to get and keep the pattern going we'll use it.
instance Monad (State s) where 
    return = pure 
    m >>= f = unfoldState arg1 arg2 where 
        arg1 = (\(m',f') -> 
                    \s -> 
                        let (mval,mstate) = runState m' s 
                        in runState (f' mval) mstate
                )
        arg2 = (m,f)

{-
Just an exemplary thing.
-}
type Stack a = [a]

pop :: State (Stack a) (Maybe a)
pop = State $ \s -> 
    case s of 
        [] -> (Nothing,s)
        (x:xs) -> (Just x,xs)

push :: a -> State (Stack a) ()
push a = State $ \s -> ((),a:s)

pushAll :: (Stack a) -> State (Stack a) ()
pushAll xs = State $ \s -> ((),xs ++ s) 

peakStack :: State (Stack a) (Stack a) 
peakStack = State $ \s -> (s,s)

data AddExpr = Val Int | Add AddExpr AddExpr
data Command = Const Int | Plus 

compile :: AddExpr -> State (Stack Command) ()
compile (Val n) = push (Const n)
compile (Add e1 e2) = do 
    push Plus 
    compile e2 
    compile e1

compileTo :: AddExpr -> Stack Command 
compileTo e = snd $ runState (compile e) []

-- For this we *should* use StateT, but that's offtopic.
evalStep :: Command -> State (Stack Int) (Maybe ())
evalStep (Const n) = do 
    push n
    return (Just ())
evalStep Plus = do 
    n1 <- pop 
    n2 <- pop
    case (n1,n2) of 
        (Just n1',Just n2') -> do 
            push (n1'+n2')
            return (Just ())
        _ -> return Nothing

evalSteps :: Stack Command -> State (Stack Int) (Maybe Int)
evalSteps [] = pop
evalSteps (c:cs) = do 
    c' <- evalStep c 
    case c' of 
        Nothing -> return Nothing 
        Just _ -> evalSteps cs

compileAndRun :: AddExpr -> Maybe Int
compileAndRun e = 
    let commands = compileTo e
    in fst $ runState (evalSteps commands) []

egExpr :: AddExpr 
egExpr = Add (Add (Val 3) (Val (-1))) (Val 2) 
egExprRun :: Maybe Int
egExprRun = compileAndRun egExpr
testegExprRun :: Bool 
testegExprRun = case egExprRun of 
    Nothing -> False 
    Just n -> n == 4

{-
Now we do Mealy machines.  These are closely related to state and streams, in particular, we can think of them as stateful stream processors -- 
and this means that given an initial state we get a stream processor.  See notes on Mealy machines for more details.
-}
data Mealy a b = M {runMealy :: a -> (b,Mealy a b)}


-- From the notes the chaining together of the transition function is the unfold.  
-- That is given the transition function and the initial state, we can unfold the 
-- chain to whatever depth required.
unfoldMealy :: ((s,a) -> (s,b)) -> s -> Mealy a b
unfoldMealy trans initS = 
    -- m_on_initS :: a -> (s,b)
    let m_on_initS = (curry trans) initS  
    -- If a :: a then m_on_initS a :: (s,b).  Thus if we abstract out the a and store the nextS and b returned from running m_on_initS on a, 
    -- we can shuffle this to unfoldingMealy on the new state and returning the value b.
    in M {runMealy = \a -> let (nextS,b) = m_on_initS a in (b,unfoldMealy trans nextS)}

-- If we were interested in Optics, Mealy a b is a profunctor.  To do: Fill in an instance of Profunctor Mealy.  This also means, by taking the 
-- collage of the profunctor Mealy a b, we get an instance of Category Mealy.  To do: Fill in this instance of category too.
-- What's neat about such Mealy machines, in Haskell, is that we can direct them to run finitely on a list, and we can also point them at a 
-- channel and let the running happen concurrently (using Control.Concurrent.STM for example).  We'll come back to running them over a 
-- channel later.  For now, we'll point them at running on a list.

-- Running a Mealy on a List gives a way to chain the Mealy instances together until the input list is exhausted.
runMealyOnList :: Mealy a b -> [a] -> [b]
runMealyOnList _m [] = []
runMealyOnList m (x:xs) = 
    let (y,m') = runMealy m x 
    in let ys = runMealyOnList m' xs 
    in (y:ys)

-- A mealy machine for enumerating the position in a list.
-- Idea for this example from https://hackage.haskell.org/package/machines-0.7.2/docs/Data-Machine-Mealy.html
mealyNumber :: Mealy Char (Int,Char)
mealyNumber = unfoldMealy (\(s,c) -> (s+1,(s,c))) 0 

-- And now we run the Mealy machine on a String which is a [Char] 
mealyNumberString :: String -> [(Int,Char)]
mealyNumberString xs = runMealyOnList mealyNumber xs

testMealyNumberString :: Bool 
testMealyNumberString = mealyNumberString "hello" == [(0,'h'),(1,'e'),(2,'l'),(3,'l'),(4,'o')]

-- EXERCISE: Allow Mealy  to have a monad threaded through them so to allow side-effecting computations.
-- The idea is that now the transition function to be unfolded should have type trans :: Monad m => (s,a) -> m (s,b)
-- And everything else should be the same.

-- EXERCISE: Add an instance for Functor and Applicative for (Mealy a) and an instance for Arrow Mealy.
-- NOTE: Once you add the instance of Arrow Mealy, you can then add {-# LANGUAGE Arrows #-} to the top of the file 
-- and then use the arrow syntax.  See https://downloads.haskell.org/~ghc/7.8.3/docs/html/users_guide/arrow-notation.html
-- Arrow syntax is sort of like do notation.

-- EXERCISE: Modify the runMealyOnList to work with streams instead.  That is write 
-- runMealyOnStream :: Mealy a b -> Stream a -> Stream b
-- This will completely enumerate the machine.

-- EXERCISE: Get Mealy machines to work in a concurrent setting, by running a mealy list on a Channel.
-- We'll do this one together later.

{-
We *could* stop with Mealy machines.  However, with Mealy machines, there's no great way to say, okay, I'm done.
If we do allow saying "Okay, I'm done" then this extended Mealy machines type is a Free Monad.  This means that 
we can later go through and speed it up using the Codensity Monad trick.  Anyways we call the resulting datatype a Generator.

Thus, a generator is just a Mealy machine that can end, and return a value of some other type c.  Intuitively, 
data Generator a b c = 
    End c 
    | Step (a -> (b,Generator a b c))

You may use this type directly, however, we want to end up getting this as a codensity monad for efficiency reasons, 
hence, we'll describe it explicitly as a Free monad.  A free monad has the following form: if f is a functor Free f is a monad.
So we are using that Generators are Free monads over the type of transition functions of Mealy machines.  
To make this explicit, suppose you have a type 

data F a = {WHATEVER} 

and an instance Functor F where {stuff}.  Then the free monad over F is defined by replacing all instances of 'a' with (Free f a) and throwing in a Pure/Return constructor.

In particular Free is defined as 

data Free f a = Pure a | Free (f (Free f a))

This is basically what we said above.  We replace the 'a' in F 'a' with Free F a and throw in a Pure constructor.  

To see how to apply this to Mealy Machines, we write down the type of Mealy again. 
data Mealy a b = M {runMealy :: a -> (b,Mealy a b)}
We can of course, write this using constructors as  
data Mealy a b = Step (a -> (b,Mealy a b))
Of course we can get runMealy back from this 
runMealy :: (Mealy a b) -> a -> (b,Mealy a b)
runMealy (Step f) = f

Great!  Now, to extend this with Pure/End and make it a monad, we abstract the Mealy a b on the right hand side of the definition 
and replace it with an arbitrary type c.
data MealyAbs a b c = Step (a -> (b,c))

We then show that MealyAbs a b is a functor, and then make the free monad on it.  When we make the free monad on Mealy a b, all the c's will 
get replaced by (Free (MealyAbs a b)) and we will get a special End command, which will, be called Pure instead.  Thus, it's pretty easy to 
show that Free (MealyAbs a b) is isomorphic to the Generator type we supplied above.  In fact, we'll show this in code.
-}
data MealyAbs a b c = Step (a -> (b,c))

instance Functor (MealyAbs a b) where 
    -- fmap :: (c -> d) -> (MealyAbs a b c) -> (MealyAbs a b d)
    fmap f (Step m) = Step $ \a -> let (b,c) = m a in (b,f c)

type Generator a b c = Free (MealyAbs a b) c

data GeneratorDirect a b c = End c | StepDirect (a -> (b,GeneratorDirect a b c))

-- data Free f a = Pure a | Free (f (Free f a))

genToGenDirect :: Generator a b c -> GeneratorDirect a b c
genToGenDirect (Pure c) = End c
-- m :: a -> (b,Generator a b c)
genToGenDirect (Free (Step m)) = StepDirect $ \a -> let (b,c) = m a in (b,genToGenDirect c)

genDirectToGen :: GeneratorDirect a b c -> Generator a b c 
genDirectToGen (End c) = Pure c
genDirectToGen (StepDirect m) = 
    let stepM a = let (b,c) = m a in (b,genDirectToGen c)
    in Free (Step stepM)

{-
Exercise: Prove that genDirectToGen and genToGenDirect are inverses of each other.
The nice bit though about Generator is that it's a Monad, and we don't have to do any work!
This is the type we want to do RNNs with, because we can specify that the the recursiveness could 
potentially end one day.  This is useful for applications like text prediction and LSTMs.
-}

{-
With Mealy Machines, we had the codata property with respect to a transition function (s,a) -> (s,b). 
Note that by currying, this is the same as s -> a -> (s,b).  For Generators, the computation from some state s, 
could give an (a -> (s,b)) or it could also end with some c.  Thus our transition function for generators is 
s -> Either c (a -> (s,b)).  Then we chain this transition function together on an initial state to unfold 
a Generator.
-}
unfoldGenerator :: (s -> Either c (a -> (s,b))) -> s -> Generator a b c
unfoldGenerator trans initS = 
    let m_on_initS = trans initS
    in case m_on_initS of 
        Left c -> Pure c
        -- m :: a -> (s,b)
        Right m -> 
            let stepM a = let (nextS,b) = m a in (b,unfoldGenerator trans nextS)
            in Free (Step stepM)

-- Another nice property of the free monad is that the starting functor embeds into it.
-- That is we have an embedding embedMealy :: MealyAbs a b c -> Free (MealyAbs a b) c and remembery Free (MealyAbs a b) c is Generator a b c
-- The embedMealy function is built-in as part of the free monad structure and is called liftF. 
embedMealy :: MealyAbs a b c -> Generator a b c
embedMealy = liftF

-- Now we can write down the yield statement.
-- Given an output, yielding gives a generator that will return it at somepoint.  This is particularly 
-- useful when combined with the monadic aspects of generators.
yield :: b -> Generator a b ()
-- The function is simply to write on MealyAbs, and then we just embed it in.
yield b = embedMealy $ Step $ \ _a -> (b,())


-- Now for the cool thing.
-- The basic idea is that programming in the Generator monad is programming in Python!  
-- Maybe we should call it the Python monad!?
doStuff :: Integer -> Generator a Integer b
doStuff i = do
    yield i 
    doStuff (i+1)

-- If the generator is a pure generator i.e. requires no inputs, then we can collect the results into a list
collectGeneratorToList :: Generator () b c -> [b]
-- recall that Pure means "end".  The ending value isn't counted (think of it more like a signal)
collectGeneratorToList (Pure _c) = []
-- m :: a -> (b,Generator a b c)
collectGeneratorToList (Free (Step m)) = 
    let (b,m') = m () 
    in b : collectGeneratorToList m'

-- Of course a generator can fail to ever terminate, so we can also extract the first n results of a  pure generator.
collectFirstGenerator :: Generator () b c -> Int -> [b] 
collectFirstGenerator g n = P.take n $ collectGeneratorToList g

collectFirstGeneratorTest :: Bool 
collectFirstGeneratorTest = collectFirstGenerator (doStuff 0) 10 == [0..9]

-- Just like with Mealy machines, we can consume values from a Generator given a list of inputs.
runGeneratorOnList :: Generator a b c -> [a] -> [b]
runGeneratorOnList (Pure _c) _xs = []
runGeneratorOnList _m [] = []
runGeneratorOnList (Free (Step m)) (x:xs) = 
    let (y,m') = m x 
    in let ys = runGeneratorOnList m' xs 
    in (y:ys)

{-
Simplified concurrent reactive round robin scheduler
-}


-- EXERCISE: Write some pretend python code by using the do notation and yield to create Generators, and then run it on a list
-- EXERCISE: As with Mealy machines, get Generators to work in a concurrent setting by having them manipulate channels
-- NOTE: Unlike with Mealy machines, Generators don't provide Stream computers, rather, they provide Colist computers...
-- Of course, in Haskell, Lists are CoLists, to the runGeneratorOnList does it.


-- EXERCISE (Challenge): Give a Generator a b c implementation through the codensity monad.  Theoretically, this is possible
-- see the paper above, so just have to learn the codensity library.
-- Also, after doing this, compare the speed.
-- Using the codensity monad is supposed to be waaaaaaaay faster.  I know they use the codensity monad for database migration 
-- stuff, and it sped the computation up by a factor of over 10million if I recall correctly...  The moral of the story is that 
-- Kan extensions are blistering.


-- EXERCISE: More long-term.  An RNN can be viewed, strictly speaking, as a Mealy machine -- in other words,  
-- an RNN is a specification of a function that maps streams to streams.  However, it could be useful to 
-- allow a more general kind of RNN that under some condition can stop.  Thus giving a generator.
-- This could be used, for example, in sentence processing where a period means "stop, the sentence is over".  
-- Of course, this kind of Generalized RNN would still need to learn over all possible sentence lengths!
-- We should dig around and see if anyone is doing this precisely, in practice.
